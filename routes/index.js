var express = require('express');
var router = express.Router();

/* GET home page. */

router.get('/', function(req, res) {
  res.render('index');
});
router.get('/:id', function(req, res) {
  res.render('images', {id:req.params.id});
});

module.exports = router;
