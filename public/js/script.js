(function() {

    "use strict";

    // Ajax Get

    var get = function(url, callback, error) {
        var xhr = new XMLHttpRequest();
        xhr.open('GET', url);
        xhr.onreadystatechange = function () {
            var DONE = 4;
            var OK = 200;
            if(xhr.readyState === DONE) {
                if(xhr.status === OK) {
                    if(typeof callback === "function") {
                        callback(JSON.parse(xhr.responseText));
                    } else if(typeof error === "function") {
                        error(xhr.status);
                    } else {
                        console.log("Error: Code "+xhr.status);
                    }
                }
            }
        };
        xhr.send(null);
    };

    // Component

    var images = function(selector, accessToken) {
        var accessToken = accessToken || 'fa07c31442508e2248fcd634a786d409428ca58d';
        var url,
            el = document.querySelector(selector),
            images = [],
            className,
            id,
            getImages,
            locale = {
                showAll: 'Показать все',
                noImages: 'Нет фотографий'
            };

        // Check element
        if(!el) {
            console.log('No element with selector '+selector);
            return;
        }
        
        // Get filial id

        id = el.dataset.id;

        if(!id) {
            console.error('data-id must be filled');
        }

        // Add CSS class

        className = el.className.split(' ');
        if(className.indexOf('flamp-images') >= 0) {
            console.log("Already initiated");
            return;
        }
        className.push('flamp-images');
        el.className = className.join(' ');

        // Get images 

        (getImages = function(link, callback) {
            var link = link || 'http://flamp.ru/api/2.0/filials/'+id+'/photos?depth=1&access_token='+accessToken;
            get(link, function(data) {
                if(data.photos.length==0) {
                    callback();
                    return;
                }

                // Add set of images
                images = images.concat(data.photos);

                // Repeat if needed
                if(data.next_link && images.length < 13) {
                    getImages(data.next_link, callback);
                } else {
                    if(images.length == 13 && data.next_link) {
                        callback(true);
                    } else {
                        callback();
                    }
                }
            });
        })(null, function(needButton) {
            console.log(images.length);
            if(images.length > 0) {

                var ul = document.createElement('ul');

                // Render list

                for(var i=0; i<images.length && i<13; i++) {
                    var li = document.createElement('li');
                    var w = (i<4 ? 300 : 50);

                    var imageUrl = images[i].image;
                    var _t = imageUrl.lastIndexOf('.');
                    imageUrl = imageUrl.substr(0,_t)+'_'+w+'_'+w+imageUrl.substr(_t);
                    li.style.cssText = 'background-image: url('+imageUrl+');';
                    ul.appendChild(li);
                }

                // Show all button

                if(images.length > 13 || needButton) {
                    var li = document.createElement('li');
                    li.innerText = locale.showAll;
                    li.onclick = function() {
                        window.location.href = 'http://flamp.ru';
                    };
                    ul.appendChild(li);
                } 

                el.appendChild(ul);
            } else {

                // No photos case

                var div = document.createElement('div');
                div.className = "noImages";
                div.innerText = locale.noImages;
                el.appendChild(div);
            }

        });

    };

    // initiate component when ready

    document.addEventListener("DOMContentLoaded", function() {
        images('#images',null);
    });

})();